/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @category dk.itu.ad.dynamicsequence
 * @package PACKAGE_NAME
 * @class Score
 * @timestamp 21-Sep-14 18:21
 * @copyright Copyright (c) 2014 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
public class Score {
    private String column;
    private String row;
    private int column_idx;
    private int row_idx;
    private int score;

    public Score(String column, int cIdx, String row, int rIdx, int score) {
        this.column = column;
        this.column_idx = cIdx;
        this.row    = row;
        this.row_idx= rIdx;
        this.score  = score;
    }

    public String getColumn() {
        return this.column;
    }

    public int getRowIdx() {
        return this.row_idx;
    }

    public int getColumnIdx() {
        return this.column_idx;
    }

    public String getRow() {
        return this.row;
    }

    public int getScore() {
        return this.score;
    }

}
