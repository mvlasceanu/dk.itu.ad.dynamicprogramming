/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @category dk.itu.ad.dynamicsequence
 * @package PACKAGE_NAME
 * @class DynamicSequence
 * @timestamp 21-Sep-14 18:49
 * @copyright Copyright (c) 2014 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
public class Alignment {
    private Sequence sequenceOne;
    private Sequence sequenceTwo;
    private String[] alignment;

    public Alignment(Sequence s1, Sequence s2) {
        this.sequenceOne = s1;
        this.sequenceTwo = s2;
        this.init();
    }

    /**
     * It computes the necessary edits
     */
    public void init() {
        int[][] matrix = new int[sequenceOne.length() + 1][sequenceTwo.length() + 1];

        for (int i = 0; i <= sequenceOne.length(); i++) {
            matrix[i][0] = i;
            //System.out.println("matrix["+i+"][0]: " + matrix[i][0]);
        }

        for (int i = 0; i <= sequenceTwo.length(); i++) {
            matrix[0][i] = i;
            //System.out.println("matrix[0]["+i+"]: " + matrix[0][i]);
        }

        for (int i = 1; i <= sequenceOne.length(); i++) {
            for (int j = 1; j <= sequenceTwo.length(); j++) {
                //System.out.println(sequenceOne.charAt(i - 1) + " <> " + sequenceTwo.charAt(j - 1));
                if (sequenceOne.charAt(i - 1) == sequenceTwo.charAt(j - 1)) {
                    //System.out.println("Before matrix["+i+"]["+j+"]: " + matrix[i][j]);
                    matrix[i][j] = matrix[i - 1][j - 1];
                    //System.out.println("After matrix["+i+"]["+j+"]: " + matrix[i][j]);
                } else {
                    //System.out.println("1Before matrix["+i+"]["+j+"]: " + matrix[i][j]);
                    matrix[i][j] = Math.min(matrix[i - 1][j], matrix[i][j - 1]) + 1;
                    //System.out.println("1After matrix["+i+"]["+j+"]: " + matrix[i][j]);
                }
            }
        }

        /*for(int x = 0; x < matrix.length; x++) {
            for(int y = 0; y < matrix[x].length; y++) {
                System.out.println("==> matrix["+x+"]["+y+"]: " + matrix[x][y]);
            }
        }*/
        StringBuilder alignmentOne = new StringBuilder(), alignmentTwo = new StringBuilder();

        for (int i = sequenceOne.length(), j = sequenceTwo.length(); i > 0 || j > 0; ) {
            if (i > 0 && matrix[i][j] == matrix[i - 1][j] + 1) {
                alignmentOne.append(sequenceOne.charAt(--i));
                alignmentTwo.append("-");
            } else if (j > 0 && matrix[i][j] == matrix[i][j - 1] + 1) {
                alignmentTwo.append(sequenceTwo.charAt(--j));
                alignmentOne.append("-");
            } else if (i > 0 && j > 0 && matrix[i][j] == matrix[i - 1][j - 1]) {
                alignmentOne.append(sequenceOne.charAt(--i));
                alignmentTwo.append(sequenceTwo.charAt(--j));
            }
        }

        this.alignment = new String[]{alignmentOne.reverse().toString(), alignmentTwo.reverse().toString()};
    }

    public String[] getAlignment() {
        return this.alignment;
    }

    public String getName() {
        return "> " + this.sequenceOne.getName() + "--" + this.sequenceTwo.getName();
    }
}
