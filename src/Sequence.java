/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @category dk.itu.ad.dynamicsequence
 * @package PACKAGE_NAME
 * @class DynamicSequence
 * @timestamp 21-Sep-14 18:44
 * @copyright Copyright (c) 2014 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
public class Sequence {
    private String name;
    private String string;

    public Sequence(String name, String string) {
        this.name = name;
        this.string = string;
    }

    public String getName() {
        return this.name;
    }

    public String getString() {
        return this.string;
    }

    public int length() {
        return this.string.length();
    }

    public char charAt(int position) {
        return this.string.charAt(position);
    }
}
