/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @category dk.itu.ad.dynamicsequence
 * @package PACKAGE_NAME
 * @class DynamicSequence
 * @timestamp 21-Sep-14 18:35
 * @copyright Copyright (c) 2014 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DynamicSequence {
    // Holds the alignments between all possible pairs
    public volatile ArrayList<Alignment> alignments = new ArrayList<>();
    // Holds all the sequences parsed from the file
    public volatile ArrayList<Sequence> sequences = new ArrayList<>();
    // Holds the scores parsed from the matrix (BLOSUM)
    public volatile ArrayList<Score> scores = new ArrayList<>();

    public static void main(String[] args) {
        // TO DO: change to parse from the command line
        String fileName = "Toy_FASTAs.in.txt";
        DynamicSequence ds = new DynamicSequence();
        ds.parseScoringTable();

        try {
            BufferedReader r = new BufferedReader(new FileReader(fileName));
            String l = r.readLine();

            String name = "";

            while(l != null) {
                String line = l.trim();
                // Sequence names
                if(line.startsWith(">")) {
                    name = line.replace(">", "");

                // Sequence strings
                } else {
                    Sequence s = new Sequence(name, line);
                    ds.sequences.add(s);
                }
                l = r.readLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create all the alignment objects
        for(int i = 0 ; i < ds.sequences.size(); i++){
            for(int j = i+1 ; j < ds.sequences.size(); j++ ){
                Alignment a = new Alignment(ds.sequences.get(i), ds.sequences.get(j));
                ds.alignments.add(a);
            }
        }

        // List the alignments
        for(Alignment a : ds.alignments) {
            System.out.println(a.getName());
            System.out.println(a.getAlignment()[0]);
            System.out.println(a.getAlignment()[1]);
        }

    }

    /**
     * Parses the score matrix
     */
    public void parseScoringTable() {
        String fileName = "BLOSUM62.txt";

        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String ln = br.readLine();

            // Holds the letter-based matrix columns
            String[] columns = null;

            // Row counter (based on line-reading
            int r = 0;

            while(ln != null ) {
                String line = ln;

                // Parses the first matrix row, the columns
                if(line.startsWith("  ")) {
                    line = ln.trim();
                    String[] ch = line.split("  ");
                    columns = new String[ch.length];
                    int c = 0;
                    for(String s : ch) {
                        String st = s.trim();
                        if(st.length() != 0) {
                            columns[c] = st;
                        }
                        c++;
                    }
                } else {
                    // Parse the rows with scores
                    if(!line.startsWith("#")) {

                        // Holds the matched scores from the regex
                        List<String> items = new ArrayList<>();

                        // Matches all the integers in the line (+/-)
                        Matcher m = Pattern.compile("([0-9]+|\\-[0-9]+)")
                                .matcher(line.trim());

                        // Add the matched integers to the list
                        while (m.find()) {
                            items.add(m.group());
                        }

                        // Add the score objects to the array
                        for(int k = 0; k < items.size(); k++) {
                            Score sc = new Score(columns[k], k, columns[r], r, Integer.parseInt(items.get(k)));
                            scores.add(sc);
                            //System.out.println("values["+r+"]["+k+"]:" + values[r][k]);
                        }

                        // Go to the next row of scores
                        r++;
                    }
                }
                ln = br.readLine();
            }


            /*for(Score s : scores){
                System.out.println("values["+s.getColumn()+"]["+s.getRow()+"]:" + s.getScore());
            }*/
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
